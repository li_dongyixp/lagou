package com.lagou.test;

import com.lagou.dao.IUserDao;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;

public class MybatisTest {

    @Test
    public void test() throws Exception {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //调用
        User user = new User();
        user.setId(1);
        user.setUsername("张三");

        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        //List<User> all = userDao.findAll();
        //for (User user1 : all) {
        //    System.out.println(user1);
        //}

        System.out.println("修改结果：" + userDao.updateUserName(user));

        System.out.println("删除结果：" + userDao.deleteUserById(1));

        System.out.println("录入结果：" + userDao.insertUser(user));
    }

}



