package com.lagou.dao;


import com.lagou.pojo.User;

import java.util.List;

public interface IUserDao {

    //查询所有用户
    public List<User> findAll() throws Exception;

    //根据条件进行用户查询
    public User findByCondition(User user) throws Exception;

    //根据传入信息进行用户数据修改
    public Integer updateUserName(User user) throws Exception;

    //根据用户ID删除用户数据
    public Integer deleteUserById(Integer id);

    //根据用户ID删除用户数据
    public Integer insertUser(User user);
}
